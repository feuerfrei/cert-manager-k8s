Setup Cert-manager cluster issuer to kubernetes
=================
Add options to your /etc/ssl/openssl.cnf (if use mac)
-
```
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment

[ v3_ca ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = critical,CA:true
```
Generate CA
-
```
openssl genrsa -out ca.key 2048 
openssl req -x509 -new -key ca.key -days 3650 -reqexts v3_req -extensions v3_ca -subj "/C=UA/ST=Kyiv/L=Kyiv/O=Company/OU=IT/CN=domain.com" -out ca.crt
```
Install cert-manager by helm3
-
```
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.4.0 \
  --set installCRDs=true
```
Create k8s secret for Cluster issuer
-
```
kubectl create secret tls ca-key-pair --cert=ca.crt --key=ca.key -n cert-manager
```
Install Cluster issuer
-
```
kubectl apply -f clusterissuer.yml
```
Check Cluster issuer
-
```
kubectl get clusterissuers.cert-manager.io
```
Test Cluster issuer
-
```
kubectl apply -f test-cert.yml
```
Teardown
-
```
helm -n cert-manager uninstall cert-manager && kubectl delete ns cert-manager
```
